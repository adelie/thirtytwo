=========================
 Changelog for thirtytwo
=========================
:Author:
  * **A. Wilcox**, documentation writer
  * **Contributors**, code
:Copyright:
  © 2020 Adélie Linux and contributors.


0.1.0 (2020-07-07)
==================

Initial release.
