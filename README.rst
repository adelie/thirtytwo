======================
 README for thirtytwo
======================
:Authors:
 * **A. Wilcox**, principal author, project manager
:Status:
 Development
:Copyright:
 © 2020 Adélie Linux.  NCSA open source license.


``thirtytwo`` is a utility allowing users to set up 32-bit environments on
64-bit Adélie Linux computers without the need to use a command line (CLI).

.. image:: https://img.shields.io/badge/chat-on%20IRC-blue.svg
   :target: ircs://irc.interlinked.me:6697/#Adelie-Support
   :alt: Chat with us on IRC: irc.interlinked.me #Adelie-Support

.. image:: https://img.shields.io/badge/license-NCSA-lightgrey.svg
   :target: LICENSE
   :alt: Licensed under NCSA

.. image:: https://img.shields.io/gitlab/pipeline/adelie/thirtytwo?gitlab_url=https%3A%2F%2Fcode.foxkit.us%2F
   :target: https://code.foxkit.us/adelie/thirtytwo/pipelines
   :alt: Click for build status



Introduction
============

This repository contains the development documentation and source code for
``thirtytwo``.


License
```````
Code is licensed under the NCSA open source license.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
``current`` branch.



If You Need Help
================

This repository is primarily for system developers.  If you're looking for
help using Adélie Linux, see `our Help Centre on the Web`_.

.. _`our Help Centre on the Web`: https://help.adelielinux.org/



Build Requirements
==================

To build ``thirtytwo``, you will need:

* Qt 5 (Core; Network; Widgets) (qt5-qtbase-dev)

* Boost.ProgramOptions (boost-dev)



Repository Layout
=================

``thirtytwo`` is laid out into multiple directories for ease of maintenance.

``assets``: Graphics and icons
``````````````````````````````
The ``assets`` directory contains UI and graphic files.


``build``: Build system artefacts
`````````````````````````````````
The ``build`` directory contains build output, including binaries and shared
libraries.


``devel``: Development information
``````````````````````````````````
The ``devel`` directory contains development documentation and specifications.
The documents are written in DocBook XML; HTML and PDF versions are not stored
in this repository.


``script``: Internal script
```````````````````````````
The ``script`` directory includes the source code for the script used to
install the environment.  This script is executed as root.


``ui``: User interface code
```````````````````````````
The ``ui`` directory includes the source code for ``thirtytwo``'s user
interface.



Contributing
============

See the CONTRIBUTING.rst_ file in the same directory as this README for
more details on how to contribute to ``thirtytwo``.

.. _CONTRIBUTING.rst: ./CONTRIBUTING.rst



Reporting Issues
================

If you have an issue using ``thirtytwo``, you may view our BTS_.  You may
also `submit an issue`_ directly.

For general discussion or questions, please use the `Adélie mailing list`_.

.. _BTS: https://bts.adelielinux.org/buglist.cgi?product=Adélie+Linux&resolution=---
.. _`submit an issue`: https://bts.adelielinux.org/enter_bug.cgi?product=Adélie+Linux
.. _`Adélie mailing list`: https://lists.adelielinux.org/postorius/lists/adelie-users.lists.adelielinux.org/

